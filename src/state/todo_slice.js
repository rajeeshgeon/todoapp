import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    todos:[],
  };
  
  export const todoSlice = createSlice({
    name: "TodoSlice",
    initialState : initialState,
    reducers: {
      addTodo(state, {payload}){
        state.todos.push(payload)
      },
      removeTodo(state, {payload}){
        state.todos = state.todos.filter((item)=>item.id != payload);
      },
    },
  });


export default todoSlice.reducer