import {configureStore} from '@reduxjs/toolkit'
import todoState from './todo_slice'

const store = configureStore({
    reducer:{
      todoState : todoState,
    },
  })
 export default store;