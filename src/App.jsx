import {Provider} from 'react-redux'
import store  from './state/store'
import TodoMain from './features/todos'

function App() {

  return (
    <Provider store={store}>
      <TodoMain/>
    </Provider>
  )
}

export default App
