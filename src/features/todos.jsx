import ComposeTodo from "./composetodo";
import TodoList from "./todolist";

export default function TodoMain(){
    return(
        <section className="todoapp">
			<ComposeTodo/>
            <TodoList/>
        </section>
    );
}