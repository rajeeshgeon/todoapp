import { useDispatch } from "react-redux";
import {todoSlice} from '../state/todo_slice'

export default function ComposeTodo(){

    const dispatch = useDispatch();
    const handleSubmit = (event) => {
        event.preventDefault();
        const todoText = event.target.todo.value;
        event.target.todo.value = "";
        const todo = {
          content: todoText,
          isCompleted: false,
          id: `id-${Date.now()}`,
        };
        dispatch(todoSlice.actions.addTodo(todo));
      };

    return(
        <header className="header">
            <form onSubmit={handleSubmit}>
            <h1>todos</h1>
            <input name="todo" className="new-todo" placeholder="What needs to be done?" autoFocus/>
            </form>
        </header>
    );
}