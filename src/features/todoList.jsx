import { useState, useRef, useEffect } from "react";
import { useDispatch,useSelector } from "react-redux";
import {todoSlice} from '../state/todo_slice'


function TodoListItem(props){
    const dispatch = useDispatch();
    const [editEnabled, setEnable] = useState(false)
    const editInputRef = useRef(null)
    const {id,content} = props.todo;
    useEffect(()=>{
        if(editEnabled){
            editInputRef.current.focus()
        }
    },[editEnabled]);

    return(
        <li  
        className={editEnabled ? "editing" : ""} 
        onDoubleClick={()=> {
            setEnable(true);
            if(editInputRef !=null){
                editInputRef.current.focus();
            }
        }}
        onBlur={()=> {
            setEnable(false)
        }}
        >
            <div className="view">
                <input className="toggle" type="checkbox"/>
                <label>{content}</label>
                <button className="destroy" onClick={
                    ()=>{dispatch(todoSlice.actions.removeTodo(id))}
                } ></button>
            </div>
            <input 
            className="edit" 
            autoFocus 
            ref={editInputRef}
            defaultValue={content}/>
        </li>   
    );
}

export default function TodoList(){
    const todos = useSelector((state)=>state.todoState.todos);
    return(
        <section className="main">
            <input id="toggle-all" className="toggle-all" type="checkbox"/>
            <label htmlFor="toggle-all">Mark all as complete</label>
            <ul className="todo-list">
                {todos.map((todo) => {
                    return <TodoListItem todo={todo} key={todo.id}/>
                })}
            </ul>
        </section>
    );
}